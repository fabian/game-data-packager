---
# Version reference:
# http://doomwiki.org/wiki/DOOM1.WAD
# http://doomwiki.org/wiki/DOOM.WAD
longname: Doom (including Ultimate Doom)
franchise: Doom
copyright: © 1993 id Software
try_repack_from:
- /usr/share/games/doom3bfg

plugin: doom_common
wiki: Doom

help_text: |
  Please provide the DOOM.WAD from either Doom 1.9 (episodes 1-3)
  or The Ultimate Doom (episodes 1-4). For the shareware version of Doom,
  activate the non-free archive component in /etc/apt/sources.list and
  "apt-get install doom-wad-shareware" instead.

  Patching older versions, or updating Doom 1.9 to The Ultimate Doom,
  is not currently supported. If you have an older version, it might
  be possible to patch it manually using dosbox.

packages:
  doom-wad:
    conflicts:
      - deb: "freedoom (<< 0.6.4-4)"
      - deb: "freedm (<< 0.6.4-4)"
    steam:
      id: 2280
      path: "common/Ultimate Doom"
    # FIXME: also look in 208200, common/DOOM 3 BFG Edition
    gog:
      url: the_ultimate_doom
      game: the_ultimate_doom_game
    # FIXME: also into doom_3_bfg_edition
    longname: Doom
    install:
    - doom.wad
    doc:
    - doom bible.pdf
    - manual.pdf

  doom-e1m4b-wad:
    expansion_for: doom-wad
    component: non-free
    longname: "Doom: Phobos Mission Control"
    copyright: © 2016 John Romero
    wiki: Phobos_Mission_Control
    engine:
      deb: prboom-plus | boom-engine
      fedora: prboom-plus
    main_wads:
      e1m4b.wad:
        args: -iwad doom.wad -file %s -warp 1 4
    install:
    - e1m4b.wad
    activated_by:
    - e1m4b.wad
    - e1m4b.zip
    license:
    - e1m4b.txt

  doom-e1m8b-wad:
    expansion_for: doom-wad
    component: non-free
    longname: "Doom: Tech Gone Bad"
    copyright: © 2016 John Romero
    wiki: Tech_Gone_Bad
    engine:
      deb: prboom-plus | boom-engine
      fedora: prboom-plus
    main_wads:
      e1m8b.wad:
        args: -iwad doom.wad -file %s -warp 1 8
    install:
    - e1m8b.wad
    activated_by:
    - e1m8b.wad
    - e1m8b.zip
    license:
    - e1m8b.txt

  doom-sigil-wad:
    expansion_for: doom-wad
    component: non-free
    longname: "Doom: SIGIL"
    copyright: © 2019 John Romero
    wiki: SIGIL
    engine:
      deb: prboom-plus | boom-engine
      fedora: prboom-plus
    main_wads:
      sigil.wad:
        args: -iwad doom.wad -file %s sigil_compat.wad
    install:
    - sigil.wad
    - sigil_compat.wad
    activated_by:
    - sigil.wad
    - sigil_v1_1.zip
    license:
    - sigil.txt

files:
  # full version
  doom.wad:
    alternatives:
    - doom.wad?1.9ud
    - doom.wad?1.9
    - doom.wad?xbox
    - doom.wad?psn
    - doom.wad?bfg
    - doom1.wad?pocketpc

  # despite its name this is full Doom
  doom1.wad?pocketpc:
    distinctive_name: false

  doom bible.pdf:
    download: http://5years.doomworld.com/doombible/doombible.pdf
    install_as: doom_bible.pdf
    look_for: [doombible.pdf, doom_bible.pdf, 'doom bible.pdf']

  setup_the_ultimate_doom_2.0.0.3.exe:
    unpack:
      format: innoextract
    provides:
    - doom.wad?1.9ud
    - doom bible.pdf
    - manual.pdf

  setup_doom_3_bfg_1.14_(13452)_(g).exe:
    other_parts:
    - setup_doom_3_bfg_1.14_(13452)_(g)-1.bin
    - setup_doom_3_bfg_1.14_(13452)_(g)-2.bin
    unpack:
      format: innoextract
    provides:
    - doom.wad?bfg

  e1m4b.zip:
    download:
      idgames-mirrors:
        path: levels/doom/Ports/d-f
    unpack:
      format: zip
    provides:
    - e1m4b.wad
    - e1m4b.txt

  e1m8b.zip:
    download:
      idgames-mirrors:
        path: levels/doom/Ports/d-f
    unpack:
      format: zip
    provides:
    - e1m8b.wad
    - e1m8b.txt

  sigil_v1_1.zip:
    download: https://www.romerogames.ie/s/SIGIL_V1_1.zip
    unpack:
      format: zip
    provides:
    - sigil.wad
    - sigil_compat.wad
    - sigil.txt

groups:
  documentation:
    doc: true
    group_members: |
      548488    c985db4e0aa6fa12dc650b04eaa041cc doom bible.pdf
      684939    a2cbc904d3ea7648b973aac7c53e9685 manual.pdf
      3060      bf5b785186a42db65ecaed2a4758ba31 e1m4b.txt
      2967      c9b317110305e81f47f64f4fc443d4a0 e1m8b.txt
      4915      dbc3cf6c1bad234b2aa292010d84e740 sigil.txt
  archives:
    group_members: |
      20575712  98a065ca917fc5ac021621098a2f80cd setup_the_ultimate_doom_2.0.0.3.exe
      779640    d93c52c6d129f93672ca2fa7665229c9 setup_doom_3_bfg_1.14_(13452)_(g).exe
      4294194686 884b37ceda0bc95a4d4cda7f9dc0ddef setup_doom_3_bfg_1.14_(13452)_(g)-1.bin
      53043836  83c57e05fc407ec3c20459d78a564519 setup_doom_3_bfg_1.14_(13452)_(g)-2.bin
      93472     863d59e8c3ef3b1e47740a08e429d9ea e1m4b.zip
      220162    69084c43103c8f897a4993afded46b1b e1m8b.zip
      3115330   9dcece46e1de87d90bcd2068408f7f74 sigil_v1_1.zip

  iwads:
    group_members: |
      # Best available full version: The Ultimate Doom
      12408292  c4fe9fd920207691a9f493668e0a2083 doom.wad?1.9ud
      # Other 1.9 variants
      11159840  1cd63c5ddff1bf8ce844237f580e9cf3 doom.wad?1.9
      12487824  fb35c4a5a9fd49ec29ab6e900572c524 doom.wad?bfg
      12474561  e4f120eab6fb410a5b6e11c947832357 doom.wad?psn
      12538385  0c8758f102ccafe26a3040bee8ba5021 doom.wad?xbox
      # despite its name this is full Doom
      14445632  3e410ecd27f61437d53fa5c279536e88 doom1.wad?pocketpc

  unsuitable:
    unsuitable: Doom 1.9 or The Ultimate Doom is required
    group_members: |
      10396254  981b03e6d1dc033301aa3095acc437ce doom1.wad?1.1
      10399316  792fd1fea023d61210857089a7c1e351 doom1.wad?1.2
      11159840  11e1cd216801ea2657723abc86ecb01f doom1.wad?1.8
      11159840  54978d12de87f162b9bcc011676cb3c0 doom1.wad?1.666
      1901322   dae9b1eea1a8e090fdfa5707187f4a43 doom1.wad?0.3
      2675669   b6afa12a8b22e2726a8ff5bd249223de doom1.wad?0.4
      3522207   9c877480b8ef33b7074f1f0c07ed6487 doom1.wad?0.5
      4196020   f0cefca49926d00903cf57551d901abe doom1.wad?shareware
      496250    740901119ba2953e3c7f3764eca6e128 doom1.wad?0.2
      5468456   049e32f18d9c9529630366cfc72726ea doom1.wad?beta

  pwads:
    group_members: |
      237271    ffce7f3a2e42ba779e211c4cf17fa591 e1m4b.wad
      566157    86e94729257b94f982dbe3e3a81e2679 e1m8b.wad
      4525740   1fe9daa0e837c7452eb2f91aac2cc983 sigil.wad
      4483345   c04912beab6aa82c114a19c976ec8c0d sigil_compat.wad

sha1sums: |
  7742089b4468a736cadb659a7deca3320fe6dcbd doom.wad?1.9
  9b07b02ab3c275a6a7570c3f73cc20d63a0e3833 doom.wad?1.9ud
  e5ec79505530e151ff0e6f517f3ce1fd65969c46 doom.wad?bfg
  117015379c529573510be08cf59810aa10bb934e doom.wad?psn
  1d1d4f69fe14fa255228d8243470678b1b4efdc5 doom.wad?xbox
  89d934616c57fe974b06c2b37a9837853a89dbbc doom1.wad?0.2
  df8ffe821a212d130ae48cf2c23721bd0ee6543b doom1.wad?0.3
  5f78b23fbffc828f5863ecff7e908d556241ff45 doom1.wad?0.4
  2c8212631b37f21ad06d18b5638c733a75e179ff doom1.wad?1.8
  2e89b86859acd9fc1e552f587b710751efcffa8e doom1.wad?1.666
  692994db9579be4201730b9ac77797fae2111bde doom1.wad?beta
  b5f86a559642a2b3bdfb8a75e91c8da97f057fe6 doom1.wad?1.2
  d3648d720b5324ce3c7bf58cf019e395911d677e doom1.wad?0.5
  df0040ccb29cc1622e74ceb3b7793a2304cca2c8 doom1.wad?1.1
  1d3c00534c452e266012e28ddc399ffe4d9ac8ed doom1.wad?pocketpc
  5b2e249b9c5133ec987b3ea77596381dc0d6bc1d doom1.wad?shareware
  923c77b2cb7b4a73abd62f62c26f614446c849ff setup_the_ultimate_doom_2.0.0.3.exe
  d6981fd46269d3bb22adf2a962e8b6c62179565e e1m4b.txt
  46215ed8988e4fea8fdb458aa45e75a4c6f2d014 e1m4b.wad
  562245983caf0d1f4efddda7b83e4b68e132f79e e1m4b.zip
  8693daabf58247c7d71a072430b31b4600f7a6c8 e1m8b.txt
  516cf84fdc85d76e0e36c8d25770344967c5cbef e1m8b.wad
  b66f68a15b8c8f02fd8d92a23040757eae706440 e1m8b.zip
  1d95e599a1587d1130bf24356a73b817ac2c1b57 sigil.wad
  60641c2519ba95565c714de09b1cf1358c4905fd sigil_compat.wad
  3a0c0bacadb3e4aab10d6fec739723ef1532f5bc sigil_v1_1.zip
...
# vim:set sw=2 sts=2 et:
